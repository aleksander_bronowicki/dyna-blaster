﻿module Dynablaster {
    export class PrefabCreator {
        createPrefabFromPool(pool, prefabConstructor, gameState, prefabName, prefabPosition, prefabProps) {
            let prefab = pool.getFirstDead();
            if (!prefab) {
                // if there is no dead prefab, create a new one
                prefab = new prefabConstructor(gameState, prefabName, prefabPosition, prefabProps);
            } else {
                // if there is a dead prefab, reset it in the new position
                prefab.reset(prefabPosition.x, prefabPosition.y);
            }
        };
    }
}