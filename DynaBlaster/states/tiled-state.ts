﻿module Dynablaster {


    export class TiledState extends Phaser.State {

        groups: { [key: string]: Phaser.Group };
        prefabs: any;
        layers: any;
        levelData: any;
        nextState: string;
        items: any;

        map: Phaser.Tilemap

        public prefabtypes = {
            "player": Player,
            "enemy": Enemy,
            "lifeItem": LifeItem,
            "bombItem": BombItem
        };


        init(levelData: any) {
            this.levelData = levelData;

            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;

            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 0;

            this.map = this.game.add.tilemap(levelData.map.key);
            let tilesetIndex = 0;
            this.map.tilesets.forEach((tileset) => {
                this.map.addTilesetImage(tileset.name, levelData.map.tilesets[tilesetIndex]);
                tilesetIndex += 1;
            });

            if (this.levelData.firstLevel) {
                localStorage.clear();
            }

            this.items = {
                lifeItem: { probability: 0.05, properties: { texture: "life_item_image", group: "items" } },
                bombItem: { probability: 0.2, properties: { texture: "bomb_item_image", group: "items" } }
            };
        }

        create() {

            this.layers = {};
            this.map.layers.forEach((layer) => {
                this.layers[layer.name] = this.map.createLayer(layer.name);
                if (layer.properties.collision) {
                    let collisionTiles = [];
                    layer.data.forEach((dataRow) => {
                        dataRow.forEach((tile) => {
                            if (tile.index > 0 && collisionTiles.indexOf(tile.index) === -1) {
                                collisionTiles.push(tile.index);
                            }
                        });
                    });
                    this.map.setCollision(collisionTiles, true, layer.name);
                }
            });
            this.layers[this.map.layer.name].resizeWorld();

            this.groups = {};
            this.levelData.groups.forEach((groupName) => {
                this.groups[groupName] = this.game.add.group();
            });

            this.prefabs = {};

            let objectLayer;
            for (objectLayer in this.map.objects) {
                this.map.objects[objectLayer].forEach(layer => this.createObject(layer));
            }

            this.initHud();
        }

        private createObject(object: any) {

            let objectY =  object.gid ? object.y - (this.map.tileHeight / 2) : object.y + (object.height / 2);
            let position = { "x": object.x + (this.map.tileHeight / 2), "y": objectY };
            let prefab;
            if (this.prefabtypes.hasOwnProperty(object.type)) {
                prefab = new this.prefabtypes[object.type](this, object.name, position, object.properties);
            }
            this.prefabs[object.name] = prefab;
        };


        private initHud() {
            let livesPosition = new Phaser.Point(0.9 * this.game.world.width, 0.03 * this.game.world.height);
            let livesProperties = { group: "hud", texture: "life_image", number_of_lives: 3 };
            let lives = new Dynablaster.Lives(this, "lives", livesPosition, livesProperties);
        };

        nextLevel() {
            const music = this.game.add.audio('next_level');
            music.play();
            localStorage.numberOfLives = this.prefabs.player.numberOfLives;
            localStorage.numberOfBombs = this.prefabs.player.numberOfBombs;
            this.game.state.start("BootState", false, false, this.levelData.next_level, "TiledState");
        };

        gameOver() {
            localStorage.clear();
            this.game.state.restart(true, false, this.levelData);
            this.game.state.start("GameOver", true, false, this.levelData );
                
        };

    }
}