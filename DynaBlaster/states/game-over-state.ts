﻿module Dynablaster {

    export class GameOverState extends Phaser.State {

        prefabs = {};
        groups = {};

        init(levelData) {
            this.groups = {};
            levelData.groups.forEach((groupName) => {
                this.groups[groupName] = this.game.add.group();
            });
        }

        preload() {
        }

        create() {


            const title_position = new Phaser.Point(0.5 * this.game.world.width, 0.3 * this.game.world.height);
            const title = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Koniec Gry",  group: "hud", font: "carrier_command", size: "32"  });
            title.anchor.setTo(0.5);
        }

        update() {
            if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.game.state.start("BootState", true, false, "assets/levels/level1.json", "TiledState");
            }
        }
    }

}