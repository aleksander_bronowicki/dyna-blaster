﻿module Dynablaster {

    export class BootState extends Phaser.State {

        private levelFilePath: string;
        nextStateName: string;

        init(levelFilePath: string, nextStateName: string) {
            this.levelFilePath = levelFilePath;
            this.nextStateName = nextStateName;
        }

        preload() {
            this.load.text("level1", this.levelFilePath);
            this.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png', 'assets/fonts/carrier_command.xml');
        }

        create() {
            const levelString = this.game.cache.getText("level1");
            const levelData = JSON.parse(levelString);
            this.game.state.start("LoadingState", true, false, levelData, this.nextStateName);
        }

    }

}