﻿module Dynablaster {

    export class MenuState extends Phaser.State {

        prefabs = {};
        groups = {};

        private startText;
        private blinkTimer;
       private menuMusic: any;

        init(levelData) {
            this.groups = {};
            levelData.groups.forEach((groupName) => {
                this.groups[groupName] = this.game.add.group();
            });
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            this.menuMusic = this.game.add.audio('menu_music');

        }

        preload() {
        }

        create() {

            var i = this.groups['hud'].add(new Phaser.Image(this.game, this.game.world.centerX, this.game.world.centerY, 'menu_background'));
            i.anchor.set(0.5);
            i.height = this.game.world.height;
            i.width = this.game.world.width;



            const title_position = new Phaser.Point(0.5 * this.game.world.width, 0.2 * this.game.world.height);

            const title = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Rozpocznij gre", size: "20", group: "hud", });

            title.anchor.setTo(0.5);

            title_position.y += 100
            const controls = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Sterowanie", size: "15", group: "hud", });
            controls.anchor.setTo(0.5);

            title_position.y += 30
            const controlsKeys = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Strzalki - Poruszanie", size: "10", group: "hud", });
            controlsKeys.anchor.setTo(0.5);
            title_position.y += 30
            const controlsSpace = new Dynablaster.TextPrefab(this, "title", title_position, { text: "SPACJA - Bomba", size: "10", group: "hud", });
            controlsSpace.anchor.setTo(0.5);

            title_position.y += 50
            this.startText = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Nacisnij SPACJE aby rozpoczac", size: "10", group: "hud", });
            this.startText.anchor.setTo(0.5);
            this.startText.visible = false;
            this.blinkTimer = this.game.time.events.loop(Phaser.Timer.SECOND * 0.4, this.blinkText, this);
            this.menuMusic.play();
        }

        private blinkText() {
            this.startText.visible = !this.startText.visible;
        }

        update() {
            if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.game.time.events.remove(this.blinkTimer);
                this.menuMusic.stop();

                this.game.state.start("BootState", true, false, "assets/levels/level1.json", "TiledState");
            }
        }

    }

}