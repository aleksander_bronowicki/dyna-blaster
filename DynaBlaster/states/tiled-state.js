var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Bomberman;
(function (Bomberman) {
    var TiledState = (function (_super) {
        __extends(TiledState, _super);
        function TiledState() {
            _super.apply(this, arguments);
            this.prefabClasses = {
                "player": Bomberman.Player
            };
        }
        TiledState.prototype.init = function (levelData) {
            var tilesetIndex;
            this.levelData = levelData;
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            // start physics system
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 0;
            // create map and set tileset
            this.map = this.game.add.tilemap(levelData.map.key);
            tilesetIndex = 0;
            this.map.tilesets.forEach(function (tileset) {
                this.map.addTilesetImage(tileset.name, levelData.map.tilesets[tilesetIndex]);
                tilesetIndex += 1;
            }, this);
        };
        TiledState.prototype.create = function () {
            var groupName, objectLayer, collisionTiles;
            // create map layers
            this.layers = {};
            this.map.layers.forEach(function (layer) {
                this.layers[layer.name] = this.map.createLayer(layer.name);
                if (layer.properties.collision) {
                    collisionTiles = [];
                    layer.data.forEach(function (dataRow) {
                        dataRow.forEach(function (tile) {
                            // check if it's a valid tile index and isn't already in the list
                            if (tile.index > 0 && collisionTiles.indexOf(tile.index) === -1) {
                                collisionTiles.push(tile.index);
                            }
                        }, this);
                    }, this);
                    this.map.setCollision(collisionTiles, true, layer.name);
                }
            }, this);
            // resize the world to be the size of the current layer
            this.layers[this.map.layer.name].resizeWorld();
            // create groups
            this.groups = {};
            this.levelData.groups.forEach(function (groupname) {
                this.groups[groupname] = this.game.add.group();
            }, this);
            this.prefabs = {};
            for (objectLayer in this.map.objects) {
                if (this.map.objects.hasOwnProperty(objectLayer)) {
                    // create layer objects
                    this.map.objects[objectLayer].forEach(this.createObject, this);
                }
            }
        };
        TiledState.prototype.createObject = function (object) {
            var object_y, position, prefab;
            // tiled coordinates starts in the bottom left corner
            object_y = (object.gid) ? object.y - (this.map.tileHeight / 2) : object.y + (object.height / 2);
            position = { "x": object.x + (this.map.tileHeight / 2), "y": object_y };
            // create object according to its type
            if (this.prefabClasses.hasOwnProperty(object.type)) {
                prefab = new this.prefabClasses[object.type](this, object.name, position, object.properties);
            }
            this.prefabs[object.name] = prefab;
        };
        ;
        return TiledState;
    }(Phaser.State));
    Bomberman.TiledState = TiledState;
})(Bomberman || (Bomberman = {}));
//# sourceMappingURL=tiled-state.js.map