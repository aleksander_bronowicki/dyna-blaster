var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Bomberman;
(function (Bomberman) {
    var LoadingState = (function (_super) {
        __extends(LoadingState, _super);
        function LoadingState() {
            _super.apply(this, arguments);
        }
        LoadingState.prototype.init = function (levelData, nextState) {
            this.levelData = levelData;
            this.nextState = nextState;
        };
        LoadingState.prototype.preload = function () {
            var assets, assetsLoader, assetKey, asset;
            assets = this.levelData.assets;
            for (assetKey in assets) {
                if (assets.hasOwnProperty(assetKey)) {
                    asset = assets[assetKey];
                    switch (asset.type) {
                        case "image":
                            this.load.image(assetKey, asset.source);
                            break;
                        case "spritesheet":
                            this.load.spritesheet(assetKey, asset.source, asset.frame_width, asset.frame_height, asset.frames, asset.margin, asset.spacing);
                            break;
                        case "tilemap":
                            this.load.tilemap(assetKey, asset.source, null, Phaser.Tilemap.TILED_JSON);
                            break;
                    }
                }
            }
        };
        LoadingState.prototype.create = function () {
            this.game.state.start(this.nextState, true, false, this.levelData);
        };
        return LoadingState;
    }(Phaser.State));
    Bomberman.LoadingState = LoadingState;
})(Bomberman || (Bomberman = {}));
//# sourceMappingURL=loading-state.js.map