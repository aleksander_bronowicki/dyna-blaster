﻿module Dynablaster {

    export class LoadingState extends Phaser.State {

        levelData: any;
        nextStateName: string;

        init(levelData: any, nextStateName: string) {
            this.levelData = levelData;
            this.nextStateName = nextStateName;
        }

        preload() {
            const assets = this.levelData.assets;
            this.loadAssets(assets);
        }

        create() {
            this.game.state.start(this.nextStateName, true, false, this.levelData);
        }

        private loadAssets(assets: any) {
            let assetKey, asset;
            for (assetKey in assets) {
                asset = assets[assetKey];
                switch (asset.type) {
                    case "image":
                        this.load.image(assetKey, asset.source);
                        break;
                    case "spritesheet":
                        this.load.spritesheet(assetKey, asset.source, asset.frameWidth, asset.frameHeight, asset.frameMax, asset.margin, asset.spacing);
                        break;
                    case "tilemap":
                        this.load.tilemap(assetKey, asset.source, null, Phaser.Tilemap.TILED_JSON);
                        break;
                    case "sound":
                        this.load.audio(assetKey, asset.source);
                        break;
                }
            }
        }

    }
}