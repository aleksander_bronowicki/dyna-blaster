var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Bomberman;
(function (Bomberman) {
    var BootState = (function (_super) {
        __extends(BootState, _super);
        function BootState() {
            _super.apply(this, arguments);
        }
        BootState.prototype.init = function (levelFile, nextState) {
            this.levelFile = levelFile;
            this.nextState = nextState;
        };
        BootState.prototype.preload = function () {
            this.load.text("level1", this.levelFile);
        };
        BootState.prototype.create = function () {
            var levelText, levelData;
            levelText = this.game.cache.getText("level1");
            levelData = JSON.parse(levelText);
            this.game.state.start("LoadingState", true, false, levelData, this.nextState);
        };
        return BootState;
    }(Phaser.State));
    Bomberman.BootState = BootState;
})(Bomberman || (Bomberman = {}));
//# sourceMappingURL=boot-state.js.map