window.onload = function () {
    var game = new Dynablaster.Game();
};
var Dynablaster;
(function (Dynablaster) {
    var Game = (function () {
        function Game() {
            this.game = new Phaser.Game(480, 480, Phaser.CANVAS);
            this.game.state.add("BootState", Dynablaster.BootState);
            this.game.state.add("LoadingState", Dynablaster.LoadingState);
            this.game.state.add("TiledState", Dynablaster.TiledState);
            this.game.state.add("GameOver", Dynablaster.GameOverState);
            this.game.state.add("MenuState", Dynablaster.MenuState);
            this.game.state.start("BootState", true, false, "assets/levels/level1.json", "MenuState");
        }
        return Game;
    }());
    Dynablaster.Game = Game;
})(Dynablaster || (Dynablaster = {}));
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Dynablaster;
(function (Dynablaster) {
    var Prefab = (function (_super) {
        __extends(Prefab, _super);
        function Prefab(gameState, name, position, properties) {
            var _this = _super.call(this, gameState.game, position.x, position.y, properties.texture) || this;
            _this.gameState = gameState;
            _this.name = name;
            _this.gameState.groups[properties.group].add(_this);
            _this.frame = +properties.frame;
            _this.gameState.prefabs[name] = _this;
            return _this;
        }
        ;
        return Prefab;
    }(Phaser.Sprite));
    Dynablaster.Prefab = Prefab;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="prefab.ts" />
var Dynablaster;
(function (Dynablaster) {
    var Item = (function (_super) {
        __extends(Item, _super);
        function Item(gameState, name, position, properties) {
            var _this = _super.call(this, gameState, name, position, properties) || this;
            _this.anchor.setTo(0.5);
            _this.gameState.game.physics.arcade.enable(_this);
            _this.body.immovable = true;
            _this.scale.setTo(0.75);
            return _this;
        }
        Item.prototype.update = function () {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.collectItem, null, this);
        };
        ;
        Item.prototype.collectItem = function (item, player) {
            var music = this.game.add.audio('collect');
            music.play();
            this.kill();
        };
        ;
        return Item;
    }(Dynablaster.Prefab));
    Dynablaster.Item = Item;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="item.ts" />
var Dynablaster;
(function (Dynablaster) {
    var BombItem = (function (_super) {
        __extends(BombItem, _super);
        function BombItem(gameState, name, position, properties) {
            var _this = _super.call(this, gameState, name, position, properties) || this;
            _this.MAXIMUM_NUMBER_OF_BOMBS = 5;
            return _this;
        }
        BombItem.prototype.update = function () {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.collectItem, null, this);
        };
        ;
        BombItem.prototype.collectItem = function (item, player) {
            _super.prototype.collectItem.call(this, item, player);
            player.numberOfBombs = Math.min(player.numberOfBombs + 1, this.MAXIMUM_NUMBER_OF_BOMBS);
        };
        ;
        return BombItem;
    }(Dynablaster.Item));
    Dynablaster.BombItem = BombItem;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="prefab.ts" />
var Dynablaster;
(function (Dynablaster) {
    var Explosion = (function (_super) {
        __extends(Explosion, _super);
        function Explosion(game_state, name, position, properties) {
            var _this = _super.call(this, game_state, name, position, properties) || this;
            _this.anchor.setTo(0.5);
            _this.duration = +properties.duration;
            _this.gameState.game.physics.arcade.enable(_this);
            _this.body.immovable = true;
            _this.scale.set(2, 1.6);
            // create the kill timer with autoDestroy equals false
            _this.killTimer = _this.gameState.time.create(false);
            _this.killTimer.add(Phaser.Timer.SECOND * _this.duration, _this.kill, _this);
            _this.killTimer.start();
            return _this;
        }
        Explosion.prototype.reset = function (position_x, position_y) {
            this.killTimer.add(Phaser.Timer.SECOND * this.duration, this.kill, this);
            return _super.prototype.reset.call(this, position_x, position_y);
        };
        ;
        return Explosion;
    }(Dynablaster.Prefab));
    Dynablaster.Explosion = Explosion;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="prefab.ts" />
/// <reference path="explosion.ts" />
var Dynablaster;
(function (Dynablaster) {
    var Bomb = (function (_super) {
        __extends(Bomb, _super);
        function Bomb(gameState, name, position, properties) {
            var _this = _super.call(this, gameState, name, position, properties) || this;
            _this.anchor.setTo(0.5);
            _this.bombRadius = +properties.bombRadius;
            _this.gameState.game.physics.arcade.enable(_this);
            _this.body.immovable = true;
            _this.scale.set(2, 2);
            _this.explodingAnimation = _this.animations.add("exploding", [0, 2, 4], 1, false);
            _this.explodingAnimation.onComplete.add(_this.kill, _this);
            _this.animations.play("exploding");
            return _this;
        }
        Bomb.prototype.reset = function (positionX, positionY, health) {
            this.explodingAnimation.restart();
            return _super.prototype.reset.call(this, positionX, positionY, health);
        };
        ;
        Bomb.prototype.kill = function () {
            var explosionPosition = new Phaser.Point(this.position.x, this.position.y);
            this.createExplosion(explosionPosition);
            this.createExplosionsInEachDirections();
            var music = this.game.add.audio('explosion_sound');
            music.play();
            this.gameState.prefabs.player.currentBombIndex -= 1;
            return _super.prototype.kill.call(this);
        };
        ;
        Bomb.prototype.createExplosionsInEachDirections = function () {
            this.creatExplosions(-1, -this.bombRadius, -1, "x");
            this.creatExplosions(1, this.bombRadius, +1, "x");
            this.creatExplosions(-1, -this.bombRadius, -1, "y");
            this.creatExplosions(1, this.bombRadius, +1, "y");
        };
        Bomb.prototype.creatExplosions = function (initialIndex, finalIndex, step, axis) {
            var index;
            for (index = initialIndex; Math.abs(index) <= Math.abs(finalIndex); index += step) {
                var explosionPosition = void 0;
                if (axis === "x") {
                    explosionPosition = new Phaser.Point(this.position.x + (index * this.width), this.position.y);
                }
                else {
                    explosionPosition = new Phaser.Point(this.position.x, this.position.y + (index * this.height));
                }
                var wallTile = this.gameState.map.getTileWorldXY(explosionPosition.x, explosionPosition.y, this.gameState.map.tileWidth, this.gameState.map.tileHeight, "walls");
                var blockTile = this.gameState.map.getTileWorldXY(explosionPosition.x, explosionPosition.y, this.gameState.map.tileWidth, this.gameState.map.tileHeight, "blocks");
                if (!wallTile && !blockTile) {
                    this.createExplosion(explosionPosition);
                }
                else {
                    if (blockTile) {
                        this.checkForItem({ x: blockTile.x * blockTile.width, y: blockTile.y * blockTile.height }, { x: blockTile.width, y: blockTile.height });
                        this.gameState.map.removeTile(blockTile.x, blockTile.y, "blocks");
                    }
                    break;
                }
            }
        };
        ;
        Bomb.prototype.createExplosion = function (position) {
            var explosionProps = { texture: "explosion_image", group: "explosions", duration: 0.1 };
            var explosionName = this.name + "_explosion_" + this.gameState.groups.explosions.countLiving();
            var explosion = new Dynablaster.PrefabCreator().createPrefabFromPool(this.gameState.groups.explosions, Dynablaster.Explosion, this.gameState, explosionName, position, explosionProps);
        };
        Bomb.prototype.checkForItem = function (blockPosition, blockSize) {
            var randomNumber = this.gameState.game.rnd.frac();
            for (var itemPrefabName in this.gameState.items) {
                if (this.gameState.items.hasOwnProperty(itemPrefabName)) {
                    var item = this.gameState.items[itemPrefabName];
                    var itemPropability = item.probability;
                    if (randomNumber < itemPropability) {
                        var itemName = this.name + "_items_" + this.gameState.groups[item.properties.group].countLiving();
                        var itemPosition = new Phaser.Point(blockPosition.x + (blockSize.x / 2), blockPosition.y + (blockSize.y / 2));
                        console.log(itemPosition);
                        var itemProperties = item.properties;
                        var itemConstructor = this.gameState.prefabtypes[itemPrefabName];
                        var itemPrefab = new Dynablaster.PrefabCreator().createPrefabFromPool(this.gameState.groups.items, itemConstructor, this.gameState, itemName, itemPosition, itemProperties);
                        break;
                    }
                }
            }
        };
        return Bomb;
    }(Dynablaster.Prefab));
    Dynablaster.Bomb = Bomb;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="prefab.ts" />
var Dynablaster;
(function (Dynablaster) {
    var Enemy = (function (_super) {
        __extends(Enemy, _super);
        function Enemy(game_state, name, position, properties) {
            var _this = _super.call(this, game_state, name, position, properties) || this;
            _this.scaleNumber = 0.8;
            _this.anchor.setTo(0.5);
            _this.scale.set(_this.scaleNumber, _this.scaleNumber);
            _this.walkingSpeed = +properties.walking_speed;
            _this.walkingDistance = +properties.walking_distance;
            _this.direction = +properties.direction;
            _this.axis = properties.axis;
            _this.previousCoord = (_this.axis === "x") ? _this.x : _this.y;
            _this.animations.add("move", [0, 1, 2], 10, true);
            _this.stoppedFrames = [1];
            _this.gameState.game.physics.arcade.enable(_this);
            if (_this.axis === "x") {
                _this.body.velocity.x = _this.direction * _this.walkingSpeed;
            }
            else {
                _this.body.velocity.y = _this.direction * _this.walkingSpeed;
            }
            return _this;
        }
        Enemy.prototype.update = function () {
            var newPosition;
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.blocks, this.switchDirection, null, this);
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.walls, this.switchDirection, null, this);
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.bombs, this.switchDirection, null, this);
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.explosions, this.kill, null, this);
            if (this.body.velocity.x < 0) {
                this.scale.setTo(-this.scaleNumber, this.scaleNumber);
                this.animations.play("move");
            }
            else if (this.body.velocity.x > 0) {
                this.scale.setTo(this.scaleNumber, this.scaleNumber);
                this.animations.play("move");
            }
            if (this.body.velocity.y < 0) {
                this.animations.play("move");
            }
            else if (this.body.velocity.y > 0) {
                this.animations.play("move");
            }
            if (this.body.velocity.x === 0 && this.body.velocity.y === 0) {
                this.animations.stop();
                this.frame = this.stoppedFrames[this.body.facing];
            }
            newPosition = (this.axis === "x") ? this.x : this.y;
            if (Math.abs(newPosition - this.previousCoord) >= this.walkingDistance) {
                this.switchDirection();
            }
        };
        ;
        Enemy.prototype.kill = function () {
            if (this.gameState.groups.enemies.countLiving() === 1) {
                var goalPosition = new Phaser.Point(this.gameState.game.world.width / 2, this.gameState.game.world.height / 2);
                var goalProperties = { texture: "portal_image", group: "portals" };
                var goal = new Dynablaster.Portal(this.gameState, "portal", goalPosition, goalProperties);
            }
            return _super.prototype.kill.call(this);
        };
        Enemy.prototype.switchDirection = function () {
            this.direction = -1 * this.direction;
            if (this.axis === "x") {
                this.previousCoord = this.x;
                this.body.velocity.x = this.direction * this.walkingSpeed;
            }
            else {
                this.previousCoord = this.y;
                this.body.velocity.y = this.direction * this.walkingSpeed;
            }
        };
        return Enemy;
    }(Dynablaster.Prefab));
    Dynablaster.Enemy = Enemy;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="item.ts" />
var Dynablaster;
(function (Dynablaster) {
    var LifeItem = (function (_super) {
        __extends(LifeItem, _super);
        function LifeItem(gameState, name, position, properties) {
            return _super.call(this, gameState, name, position, properties) || this;
        }
        LifeItem.prototype.update = function () {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.collectItem, null, this);
        };
        ;
        LifeItem.prototype.collectItem = function (item, player) {
            _super.prototype.collectItem.call(this, item, player);
            player.numberOfLives += 1;
        };
        ;
        return LifeItem;
    }(Dynablaster.Item));
    Dynablaster.LifeItem = LifeItem;
})(Dynablaster || (Dynablaster = {}));
var Dynablaster;
(function (Dynablaster) {
    var TextPrefab = (function (_super) {
        __extends(TextPrefab, _super);
        function TextPrefab(gameState, name, position, properties) {
            var _this = _super.call(this, gameState.game, position.x, position.y, "carrier_command", properties.text, properties.size) || this;
            _this.gameState = gameState;
            _this.name = name;
            if (properties.tint)
                _this.tint = properties.tint;
            _this.gameState.groups[properties.group].add(_this);
            _this.gameState.prefabs[name] = _this;
            return _this;
        }
        return TextPrefab;
    }(Phaser.BitmapText));
    Dynablaster.TextPrefab = TextPrefab;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="prefab.ts" />
/// <reference path="text.ts" />
var Dynablaster;
(function (Dynablaster) {
    var Lives = (function (_super) {
        __extends(Lives, _super);
        function Lives(gameState, name, position, properties) {
            var _this = _super.call(this, gameState, name, position, properties) || this;
            _this.fixedToCamera = true;
            _this.anchor.setTo(0.5);
            _this.scale.setTo(0.9);
            _this.textPosition = new Phaser.Point(_this.position.x + 35, _this.position.y);
            _this.textProperties = { group: "hud", text: null, size: 25, tint: 0x111111 };
            _this.text = new Dynablaster.TextPrefab(_this.gameState, "lives_text", _this.textPosition, _this.textProperties);
            _this.text.anchor.setTo(0.5);
            return _this;
        }
        Lives.prototype.update = function () {
            if (this.gameState.prefabs.player) {
                this.numberOfLives = this.gameState.prefabs.player.numberOfLives;
                this.text.text = this.numberOfLives.toString();
            }
        };
        return Lives;
    }(Dynablaster.Prefab));
    Dynablaster.Lives = Lives;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="prefab.ts" />
var Dynablaster;
(function (Dynablaster) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(gameState, name, position, properties) {
            var _this = _super.call(this, gameState, name, position, properties) || this;
            _this.currentBombIndex = 0;
            _this.scaleNumber = 0.9;
            _this.moveAniamtions = ['walkingDown', 'walkingLeft', 'walkingRight', 'walkingUp'];
            _this.waiting = false;
            _this.anchor.setTo(0.5);
            _this.scale.setTo(_this.scaleNumber, _this.scaleNumber);
            _this.walkingSPeed = +properties.walking_speed;
            _this.bombDuration = +properties.bomb_duration;
            _this.animations.add("walkingDown", [5, 4, 3], 10, true);
            _this.animations.add("walkingLeft", [0, 1, 2], 10, true);
            _this.animations.add("walkingRight", [0, 1, 2], 10, true);
            _this.animations.add("walkingUp", [6, 7], 10, true);
            _this.animations.add("blink", [5, 8], 5, true);
            _this.animations.add("reset", [5], 1, false);
            _this.gameState.game.physics.arcade.enable(_this);
            _this.cursors = _this.gameState.game.input.keyboard.createCursorKeys();
            _this.dieSound = _this.game.add.audio('fail');
            _this.initialPosition = new Phaser.Point(_this.x, _this.y);
            _this.numberOfLives = localStorage.numberOfLives || +properties.number_of_lives;
            _this.numberOfBombs = localStorage.numberOfLives || +properties.number_of_bombs;
            return _this;
        }
        ;
        Player.prototype.update = function () {
            var _this = this;
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.blocks);
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.walls);
            this.gameState.game.physics.arcade.collide(this, this.gameState.groups['bombs']);
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups['explosions'], this.die, null, this);
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups['enemies'], this.die, null, this);
            if (this.waiting)
                return;
            if (this.cursors.left.isDown && this.body.velocity.x <= 0) {
                this.movePlayerLeft();
            }
            else if (this.cursors.right.isDown && this.body.velocity.x >= 0) {
                this.movePlayerRight();
            }
            else {
                this.body.velocity.x = 0;
            }
            if (this.cursors.up.isDown && this.body.velocity.y <= 0) {
                this.movePlayerUp();
            }
            else if (this.cursors.down.isDown && this.body.velocity.y >= 0) {
                this.movePlayerDown();
            }
            else {
                this.body.velocity.y = 0;
            }
            if (this.body.velocity.x === 0 && this.body.velocity.y === 0) {
                this.moveAniamtions.forEach(function (animation) {
                    _this.animations.stop(animation);
                });
            }
            if (this.gameState.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.currentBombIndex < this.numberOfBombs) {
                var collidingBombs = this.gameState.game.physics.arcade.getObjectsAtLocation(this.x, this.y, this.gameState.groups.bombs);
                if (collidingBombs.length === 0) {
                    this.dropBomb();
                }
            }
        };
        Player.prototype.movePlayerLeft = function () {
            this.body.velocity.x = -this.walkingSPeed;
            if (this.body.velocity.y === 0) {
                this.scale.setTo(-this.scaleNumber, this.scaleNumber);
                this.animations.play("walkingLeft");
            }
        };
        Player.prototype.movePlayerRight = function () {
            this.body.velocity.x = +this.walkingSPeed;
            if (this.body.velocity.y === 0) {
                this.scale.setTo(this.scaleNumber, this.scaleNumber);
                this.animations.play("walkingRight");
            }
        };
        Player.prototype.movePlayerUp = function () {
            this.body.velocity.y = -this.walkingSPeed;
            if (this.body.velocity.x === 0) {
                this.animations.play("walkingUp");
            }
        };
        Player.prototype.movePlayerDown = function () {
            this.body.velocity.y = +this.walkingSPeed;
            if (this.body.velocity.x === 0) {
                this.animations.play("walkingDown");
            }
        };
        Player.prototype.dropBomb = function () {
            var bombName = this.name + "_bomb_" + this.gameState.groups['bombs'].countLiving();
            var bombPosition = new Phaser.Point(this.x, this.y);
            var bombProperties = { "texture": "bomb_spritesheet", "group": "bombs", bombRadius: 3 };
            var bomb = new Dynablaster.PrefabCreator().createPrefabFromPool(this.gameState.groups.bombs, Dynablaster.Bomb.prototype.constructor, this.gameState, bombName, bombPosition, bombProperties);
            this.currentBombIndex++;
        };
        ;
        Player.prototype.die = function () {
            this.dieSound.play();
            this.body.velocity.y = 0;
            this.body.velocity.x = 0;
            this.x = this.initialPosition.x;
            this.y = this.initialPosition.y;
            if (!this.waiting) {
                this.waiting = true;
                this.animations.play("blink");
                this.numberOfLives -= 1;
                if (this.gameState.prefabs.lives.numberOfLives <= 0) {
                    this.gameState.gameOver();
                }
                else {
                    var resetTimer = this.gameState.time.create(false);
                    resetTimer.add(Phaser.Timer.SECOND * 1, this.resetPosition, this);
                    resetTimer.start();
                }
            }
        };
        ;
        Player.prototype.resetPosition = function () {
            this.animations.stop();
            this.animations.play("reset");
            this.waiting = false;
        };
        return Player;
    }(Dynablaster.Prefab));
    Dynablaster.Player = Player;
})(Dynablaster || (Dynablaster = {}));
/// <reference path="prefab.ts" />
var Dynablaster;
(function (Dynablaster) {
    var Portal = (function (_super) {
        __extends(Portal, _super);
        function Portal(game_state, name, position, properties) {
            var _this = _super.call(this, game_state, name, position, properties) || this;
            _this.anchor.setTo(0.5);
            _this.scale.setTo(2);
            _this.gameState.game.physics.arcade.enable(_this);
            _this.body.immovable = true;
            return _this;
        }
        Portal.prototype.update = function () {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.reachGoal, null, this);
        };
        Portal.prototype.reachGoal = function () {
            console.log("NASTEPNY LEVEL");
            this.gameState.nextLevel();
        };
        return Portal;
    }(Dynablaster.Prefab));
    Dynablaster.Portal = Portal;
})(Dynablaster || (Dynablaster = {}));
var Dynablaster;
(function (Dynablaster) {
    var BootState = (function (_super) {
        __extends(BootState, _super);
        function BootState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        BootState.prototype.init = function (levelFilePath, nextStateName) {
            this.levelFilePath = levelFilePath;
            this.nextStateName = nextStateName;
        };
        BootState.prototype.preload = function () {
            this.load.text("level1", this.levelFilePath);
            this.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png', 'assets/fonts/carrier_command.xml');
        };
        BootState.prototype.create = function () {
            var levelString = this.game.cache.getText("level1");
            var levelData = JSON.parse(levelString);
            this.game.state.start("LoadingState", true, false, levelData, this.nextStateName);
        };
        return BootState;
    }(Phaser.State));
    Dynablaster.BootState = BootState;
})(Dynablaster || (Dynablaster = {}));
var Dynablaster;
(function (Dynablaster) {
    var GameOverState = (function (_super) {
        __extends(GameOverState, _super);
        function GameOverState() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.prefabs = {};
            _this.groups = {};
            return _this;
        }
        GameOverState.prototype.init = function (levelData) {
            var _this = this;
            this.groups = {};
            levelData.groups.forEach(function (groupName) {
                _this.groups[groupName] = _this.game.add.group();
            });
        };
        GameOverState.prototype.preload = function () {
        };
        GameOverState.prototype.create = function () {
            var title_position = new Phaser.Point(0.5 * this.game.world.width, 0.3 * this.game.world.height);
            var title = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Koniec Gry", group: "hud", font: "carrier_command", size: "32" });
            title.anchor.setTo(0.5);
        };
        GameOverState.prototype.update = function () {
            if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.game.state.start("BootState", true, false, "assets/levels/level1.json", "TiledState");
            }
        };
        return GameOverState;
    }(Phaser.State));
    Dynablaster.GameOverState = GameOverState;
})(Dynablaster || (Dynablaster = {}));
var Dynablaster;
(function (Dynablaster) {
    var LoadingState = (function (_super) {
        __extends(LoadingState, _super);
        function LoadingState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        LoadingState.prototype.init = function (levelData, nextStateName) {
            this.levelData = levelData;
            this.nextStateName = nextStateName;
        };
        LoadingState.prototype.preload = function () {
            var assets = this.levelData.assets;
            this.loadAssets(assets);
        };
        LoadingState.prototype.create = function () {
            this.game.state.start(this.nextStateName, true, false, this.levelData);
        };
        LoadingState.prototype.loadAssets = function (assets) {
            var assetKey, asset;
            for (assetKey in assets) {
                asset = assets[assetKey];
                switch (asset.type) {
                    case "image":
                        this.load.image(assetKey, asset.source);
                        break;
                    case "spritesheet":
                        this.load.spritesheet(assetKey, asset.source, asset.frameWidth, asset.frameHeight, asset.frameMax, asset.margin, asset.spacing);
                        break;
                    case "tilemap":
                        this.load.tilemap(assetKey, asset.source, null, Phaser.Tilemap.TILED_JSON);
                        break;
                    case "sound":
                        this.load.audio(assetKey, asset.source);
                        break;
                }
            }
        };
        return LoadingState;
    }(Phaser.State));
    Dynablaster.LoadingState = LoadingState;
})(Dynablaster || (Dynablaster = {}));
var Dynablaster;
(function (Dynablaster) {
    var MenuState = (function (_super) {
        __extends(MenuState, _super);
        function MenuState() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.prefabs = {};
            _this.groups = {};
            return _this;
        }
        MenuState.prototype.init = function (levelData) {
            var _this = this;
            this.groups = {};
            levelData.groups.forEach(function (groupName) {
                _this.groups[groupName] = _this.game.add.group();
            });
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            this.menuMusic = this.game.add.audio('menu_music');
        };
        MenuState.prototype.preload = function () {
        };
        MenuState.prototype.create = function () {
            var i = this.groups['hud'].add(new Phaser.Image(this.game, this.game.world.centerX, this.game.world.centerY, 'menu_background'));
            i.anchor.set(0.5);
            i.height = this.game.world.height;
            i.width = this.game.world.width;
            var title_position = new Phaser.Point(0.5 * this.game.world.width, 0.2 * this.game.world.height);
            var title = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Rozpocznij gre", size: "20", group: "hud", });
            title.anchor.setTo(0.5);
            title_position.y += 100;
            var controls = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Sterowanie", size: "15", group: "hud", });
            controls.anchor.setTo(0.5);
            title_position.y += 30;
            var controlsKeys = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Strzalki - Poruszanie", size: "10", group: "hud", });
            controlsKeys.anchor.setTo(0.5);
            title_position.y += 30;
            var controlsSpace = new Dynablaster.TextPrefab(this, "title", title_position, { text: "SPACJA - Bomba", size: "10", group: "hud", });
            controlsSpace.anchor.setTo(0.5);
            title_position.y += 50;
            this.startText = new Dynablaster.TextPrefab(this, "title", title_position, { text: "Nacisnij SPACJE aby rozpoczac", size: "10", group: "hud", });
            this.startText.anchor.setTo(0.5);
            this.startText.visible = false;
            this.blinkTimer = this.game.time.events.loop(Phaser.Timer.SECOND * 0.4, this.blinkText, this);
            this.menuMusic.play();
        };
        MenuState.prototype.blinkText = function () {
            this.startText.visible = !this.startText.visible;
        };
        MenuState.prototype.update = function () {
            if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.game.time.events.remove(this.blinkTimer);
                this.menuMusic.stop();
                this.game.state.start("BootState", true, false, "assets/levels/level1.json", "TiledState");
            }
        };
        return MenuState;
    }(Phaser.State));
    Dynablaster.MenuState = MenuState;
})(Dynablaster || (Dynablaster = {}));
var Dynablaster;
(function (Dynablaster) {
    var TiledState = (function (_super) {
        __extends(TiledState, _super);
        function TiledState() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.prefabtypes = {
                "player": Dynablaster.Player,
                "enemy": Dynablaster.Enemy,
                "lifeItem": Dynablaster.LifeItem,
                "bombItem": Dynablaster.BombItem
            };
            return _this;
        }
        TiledState.prototype.init = function (levelData) {
            var _this = this;
            this.levelData = levelData;
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 0;
            this.map = this.game.add.tilemap(levelData.map.key);
            var tilesetIndex = 0;
            this.map.tilesets.forEach(function (tileset) {
                _this.map.addTilesetImage(tileset.name, levelData.map.tilesets[tilesetIndex]);
                tilesetIndex += 1;
            });
            if (this.levelData.firstLevel) {
                localStorage.clear();
            }
            this.items = {
                lifeItem: { probability: 0.1, properties: { texture: "life_item_image", group: "items" } },
                bombItem: { probability: 0.3, properties: { texture: "bomb_item_image", group: "items" } }
            };
        };
        TiledState.prototype.create = function () {
            var _this = this;
            this.layers = {};
            this.map.layers.forEach(function (layer) {
                _this.layers[layer.name] = _this.map.createLayer(layer.name);
                if (layer.properties.collision) {
                    var collisionTiles_1 = [];
                    layer.data.forEach(function (dataRow) {
                        dataRow.forEach(function (tile) {
                            if (tile.index > 0 && collisionTiles_1.indexOf(tile.index) === -1) {
                                collisionTiles_1.push(tile.index);
                            }
                        });
                    });
                    _this.map.setCollision(collisionTiles_1, true, layer.name);
                }
            });
            this.layers[this.map.layer.name].resizeWorld();
            this.groups = {};
            this.levelData.groups.forEach(function (groupName) {
                _this.groups[groupName] = _this.game.add.group();
            });
            this.prefabs = {};
            var objectLayer;
            for (objectLayer in this.map.objects) {
                this.map.objects[objectLayer].forEach(function (layer) { return _this.createObject(layer); });
            }
            this.initHud();
        };
        TiledState.prototype.createObject = function (object) {
            var objectY = object.gid ? object.y - (this.map.tileHeight / 2) : object.y + (object.height / 2);
            var position = { "x": object.x + (this.map.tileHeight / 2), "y": objectY };
            var prefab;
            if (this.prefabtypes.hasOwnProperty(object.type)) {
                prefab = new this.prefabtypes[object.type](this, object.name, position, object.properties);
            }
            this.prefabs[object.name] = prefab;
        };
        ;
        TiledState.prototype.initHud = function () {
            var livesPosition = new Phaser.Point(0.9 * this.game.world.width, 0.03 * this.game.world.height);
            var livesProperties = { group: "hud", texture: "life_image", number_of_lives: 3 };
            var lives = new Dynablaster.Lives(this, "lives", livesPosition, livesProperties);
        };
        ;
        TiledState.prototype.nextLevel = function () {
            var music = this.game.add.audio('next_level');
            music.play();
            localStorage.numberOfLives = this.prefabs.player.numberOfLives;
            localStorage.numberOfBombs = this.prefabs.player.numberOfBombs;
            this.game.state.start("BootState", false, false, this.levelData.next_level, "TiledState");
        };
        ;
        TiledState.prototype.gameOver = function () {
            localStorage.clear();
            this.game.state.restart(true, false, this.levelData);
            this.game.state.start("GameOver", true, false, this.levelData);
        };
        ;
        return TiledState;
    }(Phaser.State));
    Dynablaster.TiledState = TiledState;
})(Dynablaster || (Dynablaster = {}));
var Dynablaster;
(function (Dynablaster) {
    var PrefabCreator = (function () {
        function PrefabCreator() {
        }
        PrefabCreator.prototype.createPrefabFromPool = function (pool, prefabConstructor, gameState, prefabName, prefabPosition, prefabProps) {
            var prefab = pool.getFirstDead();
            if (!prefab) {
                // if there is no dead prefab, create a new one
                prefab = new prefabConstructor(gameState, prefabName, prefabPosition, prefabProps);
            }
            else {
                // if there is a dead prefab, reset it in the new position
                prefab.reset(prefabPosition.x, prefabPosition.y);
            }
        };
        ;
        return PrefabCreator;
    }());
    Dynablaster.PrefabCreator = PrefabCreator;
})(Dynablaster || (Dynablaster = {}));
