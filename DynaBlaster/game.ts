﻿module Dynablaster {

   export class Game {

        constructor() {
            this.game = new Phaser.Game(480, 480, Phaser.CANVAS);
            this.game.state.add("BootState",  BootState);
            this.game.state.add("LoadingState",  LoadingState);
            this.game.state.add("TiledState", TiledState); 
            this.game.state.add("GameOver", GameOverState);
            this.game.state.add("MenuState", MenuState);

            this.game.state.start("BootState", true, false, "assets/levels/level1.json", "MenuState");
        }

        game: Phaser.Game;
    }

}

