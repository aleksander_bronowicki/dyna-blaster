﻿/// <reference path="prefab.ts" />
module Dynablaster {

    export class Explosion extends Prefab{
        killTimer: Phaser.Timer;
        duration: number;

        constructor(game_state, name, position, properties) {
            super(game_state, name, position, properties);

            this.anchor.setTo(0.5);

            this.duration = +properties.duration;

            this.gameState.game.physics.arcade.enable(this);
            this.body.immovable = true;
            this.scale.set(2,1.6);

            // create the kill timer with autoDestroy equals false
            this.killTimer = this.gameState.time.create(false);
            this.killTimer.add(Phaser.Timer.SECOND * this.duration, this.kill, this);
            this.killTimer.start();
        }

      


        reset(position_x, position_y) {
        
            this.killTimer.add(Phaser.Timer.SECOND * this.duration, this.kill, this);
            return super.reset(position_x, position_y);
        };
    }
}