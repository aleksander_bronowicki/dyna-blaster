﻿/// <reference path="prefab.ts" />
module Dynablaster {

    export class Player extends Dynablaster.Prefab {

        public numberOfLives: number;
        public numberOfBombs: number;

        private walkingSPeed: number;
        private bombDuration: number;
        private cursors: any;
        private initialPosition: Phaser.Point;
        private currentBombIndex = 0;
        private scaleNumber = 0.9;
        private dieSound;
        private moveAniamtions: string[] = ['walkingDown', 'walkingLeft', 'walkingRight', 'walkingUp'];

        constructor(gameState, name, position, properties) {
            super(gameState, name, position, properties);

            this.anchor.setTo(0.5);
            this.scale.setTo(this.scaleNumber, this.scaleNumber);
            this.walkingSPeed = +properties.walking_speed;
            this.bombDuration = +properties.bomb_duration;

            this.animations.add("walkingDown", [5, 4, 3], 10, true);
            this.animations.add("walkingLeft", [0, 1, 2], 10, true);
            this.animations.add("walkingRight", [0, 1, 2], 10, true);
            this.animations.add("walkingUp", [6, 7], 10, true);
            this.animations.add("blink", [5, 8], 5, true);
            this.animations.add("reset", [5], 1, false);

            this.gameState.game.physics.arcade.enable(this);

            this.cursors = this.gameState.game.input.keyboard.createCursorKeys();
            this.dieSound = this.game.add.audio('fail');


            this.initialPosition = new Phaser.Point(this.x, this.y);
            this.numberOfLives = localStorage.numberOfLives || +properties.number_of_lives;
            this.numberOfBombs = localStorage.numberOfLives || +properties.number_of_bombs;
        };

        update() {
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.blocks);
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.walls);
            this.gameState.game.physics.arcade.collide(this, this.gameState.groups['bombs']);
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups['explosions'], this.die, null, this);
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups['enemies'], this.die, null, this);

            if (this.waiting)
                return;

            if (this.cursors.left.isDown && this.body.velocity.x <= 0) {
                this.movePlayerLeft();

            } else if (this.cursors.right.isDown && this.body.velocity.x >= 0) {
                this.movePlayerRight();
            } else {
                this.body.velocity.x = 0;
            }

            if (this.cursors.up.isDown && this.body.velocity.y <= 0) {
                this.movePlayerUp();
            } else if (this.cursors.down.isDown && this.body.velocity.y >= 0) {
                this.movePlayerDown();
            } else {
                this.body.velocity.y = 0;
            }

            if (this.body.velocity.x === 0 && this.body.velocity.y === 0) {
                this.moveAniamtions.forEach((animation) => {
                    this.animations.stop(animation);
                });
            }

            if (this.gameState.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.currentBombIndex < this.numberOfBombs) {
                const collidingBombs = this.gameState.game.physics.arcade.getObjectsAtLocation(this.x, this.y, this.gameState.groups.bombs);
                if (collidingBombs.length === 0) {
                    this.dropBomb();
                }
            }


        }

        private movePlayerLeft() {
            this.body.velocity.x = -this.walkingSPeed;
            if (this.body.velocity.y === 0) {
                this.scale.setTo(-this.scaleNumber, this.scaleNumber);
                this.animations.play("walkingLeft");
            }
        }

        private movePlayerRight() {
            this.body.velocity.x = +this.walkingSPeed;
            if (this.body.velocity.y === 0) {
                this.scale.setTo(this.scaleNumber, this.scaleNumber);
                this.animations.play("walkingRight");
            }
        }

        private movePlayerUp() {
            this.body.velocity.y = -this.walkingSPeed;
            if (this.body.velocity.x === 0) {
                this.animations.play("walkingUp");
            }
        }

        private movePlayerDown() {
            this.body.velocity.y = +this.walkingSPeed;
            if (this.body.velocity.x === 0) {
                this.animations.play("walkingDown");
            }
        }
        private dropBomb() {
            const bombName = this.name + "_bomb_" + this.gameState.groups['bombs'].countLiving();
            const bombPosition = new Phaser.Point(this.x, this.y);
            const bombProperties = { "texture": "bomb_spritesheet", "group": "bombs", bombRadius: 3 };
            const bomb = new Dynablaster.PrefabCreator().createPrefabFromPool(this.gameState.groups.bombs, Dynablaster.Bomb.prototype.constructor, this.gameState, bombName, bombPosition, bombProperties);
            this.currentBombIndex++;
        };

        private waiting = false;

        private die() {
            this.dieSound.play();
            this.body.velocity.y = 0;
            this.body.velocity.x = 0;
            this.x = this.initialPosition.x;
            this.y = this.initialPosition.y;
            if (!this.waiting) {
                this.waiting = true;
                this.animations.play("blink");
                this.numberOfLives -= 1;
                if (this.gameState.prefabs.lives.numberOfLives <= 0) {
                    this.gameState.gameOver();
                } else {
                    const resetTimer = this.gameState.time.create(false);
                    resetTimer.add(Phaser.Timer.SECOND * 1, this.resetPosition, this);
                    resetTimer.start();
                }
            }
        };

        private resetPosition() {
          
            this.animations.stop();
            this.animations.play("reset");
            this.waiting = false;

        }

    }
}