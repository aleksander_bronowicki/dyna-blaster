﻿/// <reference path="item.ts" />
module Dynablaster {
    export class LifeItem extends Item {
        constructor(gameState, name, position, properties) {
            super(gameState, name, position, properties);

        }

        update() {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.collectItem, null, this);
        };

        collectItem(item, player : Player) {
            super.collectItem(item, player)
            player.numberOfLives += 1;
        };

    }
}