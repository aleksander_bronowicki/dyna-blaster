﻿/// <reference path="prefab.ts" />
module Dynablaster {
    export class Item extends Prefab {
        constructor(gameState, name, position, properties) {
            super(gameState, name, position, properties);

            this.anchor.setTo(0.5);

            this.gameState.game.physics.arcade.enable(this);
            this.body.immovable = true;

            this.scale.setTo(0.75);
        }

        update() {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.collectItem, null, this);
        };

        collectItem(item, player: Player) {
            const music = this.game.add.audio('collect');
            music.play();
            this.kill();
        };

    }
}