var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Bomberman;
(function (Bomberman) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game_state, name, position, properties) {
            _super.call(this, game_state, name, position, properties);
            this.anchor.setTo(0.5);
            this.walking_speed = +properties.walking_speed;
            this.bomb_duration = +properties.bomb_duration;
            this.dropping_bomb = false;
            this.animations.add("walking_down", [1, 2, 3], 10, true);
            this.animations.add("walking_left", [4, 5, 6, 7], 10, true);
            this.animations.add("walking_right", [4, 5, 6, 7], 10, true);
            this.animations.add("walking_up", [0, 8, 9], 10, true);
            this.stopped_frames = [1, 4, 4, 0, 1];
            this.game_state.game.physics.arcade.enable(this);
            this.body.setSize(14, 12, 0, 4);
            this.cursors = this.game_state.game.input.keyboard.createCursorKeys();
        }
        ;
        return Player;
    }(Bomberman.Prefab));
    Bomberman.Player = Player;
})(Bomberman || (Bomberman = {}));
//# sourceMappingURL=player.js.map