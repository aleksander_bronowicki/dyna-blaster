﻿module Dynablaster {
    export class TextPrefab extends Phaser.BitmapText {

        public gameState: TiledState;

        constructor(gameState, name, position, properties) {
            super(gameState.game, position.x, position.y, "carrier_command", properties.text, properties.size);

            this.gameState = gameState;
            this.name = name;
            if (properties.tint)
                this.tint = properties.tint;
            this.gameState.groups[properties.group].add(this);

            this.gameState.prefabs[name] = this;

        }
    }
}