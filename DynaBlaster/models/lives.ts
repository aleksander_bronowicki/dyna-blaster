﻿/// <reference path="prefab.ts" />
/// <reference path="text.ts" />

module Dynablaster {

    export class Lives extends Dynablaster.Prefab {
        textPosition: Phaser.Point;
        textStyle: any;
        textProperties: any;
        text: Dynablaster.TextPrefab;
        clockPosition: Phaser.Point;
        clockText: Dynablaster.TextPrefab;
        public numberOfLives: number;

        constructor(gameState, name, position, properties) {
            super(gameState, name, position, properties);

            this.fixedToCamera = true;

            this.anchor.setTo(0.5);
            this.scale.setTo(0.9);

            this.textPosition = new Phaser.Point(this.position.x + 35, this.position.y);
            this.textProperties = { group: "hud", text: null, size: 25, tint: 0x111111 };
            this.text = new Dynablaster.TextPrefab(this.gameState, "lives_text", this.textPosition, this.textProperties);
            this.text.anchor.setTo(0.5);
     

        }

        update() {
            if (this.gameState.prefabs.player) {
                this.numberOfLives = this.gameState.prefabs.player.numberOfLives;
                this.text.text = this.numberOfLives.toString();
            }
        }
    }
}