﻿module Dynablaster {

    export class Prefab extends Phaser.Sprite {

        public gameState: TiledState;
        constructor(gameState, name, position, properties) {
            super(gameState.game, position.x, position.y, properties.texture);

            this.gameState = gameState;

            this.name = name;

            this.gameState.groups[properties.group].add(this);
            this.frame = +properties.frame;
            this.gameState.prefabs[name] = this;
        };

    }
}