var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Bomberman;
(function (Bomberman) {
    var Prefab = (function (_super) {
        __extends(Prefab, _super);
        function Prefab(game_state, name, position, properties) {
            _super.call(this, game_state.game, position.x, position.y, properties.texture);
            this.game_state = game_state;
            this.name = name;
            this.game_state.groups[properties.group].add(this);
            this.frame = +properties.frame;
            this.game_state.prefabs[name] = this;
        }
        ;
        return Prefab;
    }(Phaser.Sprite));
    Bomberman.Prefab = Prefab;
})(Bomberman || (Bomberman = {}));
//# sourceMappingURL=prefab.js.map