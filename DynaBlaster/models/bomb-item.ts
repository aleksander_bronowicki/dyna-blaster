﻿/// <reference path="item.ts" />

module Dynablaster {
    export class BombItem extends Item {
        MAXIMUM_NUMBER_OF_BOMBS = 5;

        constructor(gameState, name, position, properties) {
            super(gameState, name, position, properties);

        }

        update() {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.collectItem, null, this);
        };

        collectItem(item, player: Player) {
            super.collectItem(item, player)
            player.numberOfBombs = Math.min(player.numberOfBombs + 1, this.MAXIMUM_NUMBER_OF_BOMBS);
        };

    }
}