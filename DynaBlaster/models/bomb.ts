﻿/// <reference path="prefab.ts" />
/// <reference path="explosion.ts" />

module Dynablaster {

    export class Bomb extends Dynablaster.Prefab {


        private bombRadius: number;
        private explodingAnimation: Phaser.Animation;

        constructor(gameState, name, position, properties) {
            super(gameState, name, position, properties);
            this.anchor.setTo(0.5);

            this.bombRadius = +properties.bombRadius;

            this.gameState.game.physics.arcade.enable(this);
            this.body.immovable = true;
            this.scale.set(2, 2);
            this.explodingAnimation = this.animations.add("exploding", [0, 2, 4], 1, false);
            this.explodingAnimation.onComplete.add(this.kill, this);
            this.animations.play("exploding");
        }

        reset(positionX: number, positionY: number, health?: number) {
            this.explodingAnimation.restart();
            return super.reset(positionX, positionY, health);
        };

        kill() {


            const explosionPosition = new Phaser.Point(this.position.x, this.position.y);
            this.createExplosion(explosionPosition);
            this.createExplosionsInEachDirections();
            const music = this.game.add.audio('explosion_sound');
            music.play();
            this.gameState.prefabs.player.currentBombIndex -= 1;
            return super.kill();
        };

        private createExplosionsInEachDirections() {
            this.creatExplosions(-1, -this.bombRadius, -1, "x");
            this.creatExplosions(1, this.bombRadius, +1, "x");
            this.creatExplosions(-1, -this.bombRadius, -1, "y");
            this.creatExplosions(1, this.bombRadius, +1, "y");
        }

        private creatExplosions(initialIndex, finalIndex, step, axis) {
            let index;
            for (index = initialIndex; Math.abs(index) <= Math.abs(finalIndex); index += step) {
                let explosionPosition;
                if (axis === "x") {
                    explosionPosition = new Phaser.Point(this.position.x + (index * this.width), this.position.y);
                } else {
                    explosionPosition = new Phaser.Point(this.position.x, this.position.y + (index * this.height));
                }

                const wallTile = this.gameState.map.getTileWorldXY(explosionPosition.x, explosionPosition.y, this.gameState.map.tileWidth, this.gameState.map.tileHeight, "walls");
                const blockTile = this.gameState.map.getTileWorldXY(explosionPosition.x, explosionPosition.y, this.gameState.map.tileWidth, this.gameState.map.tileHeight, "blocks");
                if (!wallTile && !blockTile) {
                    this.createExplosion(explosionPosition);
                } else {
                    if (blockTile) {
                        this.checkForItem({ x: blockTile.x * blockTile.width, y: blockTile.y * blockTile.height },
                            { x: blockTile.width, y: blockTile.height });
                        this.gameState.map.removeTile(blockTile.x, blockTile.y, "blocks");
                    }
                    break;
                }
            }
        };

        private createExplosion(position: Phaser.Point) {
            const explosionProps = { texture: "explosion_image", group: "explosions", duration: 0.1  };
            let explosionName = this.name + "_explosion_" + this.gameState.groups.explosions.countLiving();
            const explosion = new Dynablaster.PrefabCreator().createPrefabFromPool(this.gameState.groups.explosions, Dynablaster.Explosion, this.gameState, explosionName, position, explosionProps);

        }

        private checkForItem(blockPosition, blockSize) {
            const randomNumber = this.gameState.game.rnd.frac();

            for (let itemPrefabName in this.gameState.items) {
                if (this.gameState.items.hasOwnProperty(itemPrefabName)) {
                    const item = this.gameState.items[itemPrefabName];
                    const itemPropability = item.probability;
                    if (randomNumber < itemPropability) {
                        const itemName = this.name + "_items_" + this.gameState.groups[item.properties.group].countLiving();
                        const itemPosition = new Phaser.Point(blockPosition.x + (blockSize.x / 2), blockPosition.y + (blockSize.y / 2));
                        console.log(itemPosition);
                        const itemProperties = item.properties;
                        const itemConstructor = this.gameState.prefabtypes[itemPrefabName];
                        const itemPrefab = new PrefabCreator().createPrefabFromPool(this.gameState.groups.items, itemConstructor, this.gameState, itemName, itemPosition, itemProperties);
                        break;
                    }
                }
            }

        }
    }
}