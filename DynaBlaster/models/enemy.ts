﻿/// <reference path="prefab.ts" />

module Dynablaster {

    export class Enemy extends Dynablaster.Prefab {

        private walkingSpeed: number;
        private walkingDistance: number;
        private direction: number;
        private axis: string;
        private previousCoord: number
        private stoppedFrames: number[];
        private scaleNumber: number = 0.8;
        constructor(game_state, name, position, properties) {

            super(game_state, name, position, properties);

            this.anchor.setTo(0.5);
            this.scale.set(this.scaleNumber, this.scaleNumber);

            this.walkingSpeed = +properties.walking_speed;
            this.walkingDistance = +properties.walking_distance;
            this.direction = +properties.direction;
            this.axis = properties.axis;

            this.previousCoord = (this.axis === "x") ? this.x : this.y;

            this.animations.add("move", [0,1,2], 10, true);
      

            this.stoppedFrames = [1];

            this.gameState.game.physics.arcade.enable(this);
            if (this.axis === "x") {
                this.body.velocity.x = this.direction * this.walkingSpeed;
            } else {
                this.body.velocity.y = this.direction * this.walkingSpeed;
            }
        }



        update() {
            var newPosition;
   
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.blocks, this.switchDirection, null, this);
            this.gameState.game.physics.arcade.collide(this, this.gameState.layers.walls, this.switchDirection, null, this);

            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.bombs, this.switchDirection, null, this);
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.explosions, this.kill, null, this);

            if (this.body.velocity.x < 0) {
                this.scale.setTo(-this.scaleNumber, this.scaleNumber);
                this.animations.play("move");
            } else if (this.body.velocity.x > 0) {
                this.scale.setTo(this.scaleNumber, this.scaleNumber);
                this.animations.play("move");
            }

            if (this.body.velocity.y < 0) {
                this.animations.play("move");
            } else if (this.body.velocity.y > 0) {
                this.animations.play("move");
            }

            if (this.body.velocity.x === 0 && this.body.velocity.y === 0) {
                this.animations.stop();
                this.frame = this.stoppedFrames[this.body.facing];
            }

            newPosition = (this.axis === "x") ? this.x : this.y;
            if (Math.abs(newPosition - this.previousCoord) >= this.walkingDistance) {
                this.switchDirection();
            }
        };

        kill() {
            if (this.gameState.groups.enemies.countLiving() === 1) {

                let goalPosition = new Phaser.Point(this.gameState.game.world.width / 2, this.gameState.game.world.height / 2);
                let goalProperties = { texture: "portal_image", group: "portals" };
                const goal = new Dynablaster.Portal(this.gameState, "portal", goalPosition, goalProperties);
            }
            return super.kill();
        }

        switchDirection() {
            this.direction = -1 * this.direction;
            if (this.axis === "x") {
                this.previousCoord = this.x;
                  this.body.velocity.x = this.direction * this.walkingSpeed;
            } else {
                this.previousCoord = this.y;
                this.body.velocity.y = this.direction * this.walkingSpeed;
            }
        }
    }
}