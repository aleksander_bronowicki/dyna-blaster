﻿/// <reference path="prefab.ts" />

module Dynablaster {
    export class Portal extends Dynablaster.Prefab {

        constructor(game_state, name, position, properties) {
            super(game_state, name, position, properties);
            this.anchor.setTo(0.5);
            this.scale.setTo(2);

            this.gameState.game.physics.arcade.enable(this);
            this.body.immovable = true;
        }

        update() {
            this.gameState.game.physics.arcade.overlap(this, this.gameState.groups.players, this.reachGoal, null, this);
        }

        reachGoal() {
            console.log("NASTEPNY LEVEL")
            this.gameState.nextLevel();
        }




    }
}

